/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.diccionarioapp.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.diccionarioapp.dao.exceptions.NonexistentEntityException;
import root.diccionarioapp.dao.exceptions.PreexistingEntityException;
import root.diccionarioapp.entity.TbDiccionario;

/**
 *
 * @author surro
 */
public class TbDiccionarioJpaController implements Serializable {

  public TbDiccionarioJpaController() {
  }
  
  private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

  public EntityManager getEntityManager() {
    return emf.createEntityManager();
  }

  public void create(TbDiccionario tbDiccionario) throws PreexistingEntityException, Exception {
    EntityManager em = null;
    try {
      em = getEntityManager();
      em.getTransaction().begin();
      em.persist(tbDiccionario);
      em.getTransaction().commit();
    } catch (Exception ex) {
      if (findTbDiccionario(tbDiccionario.getId()) != null) {
        throw new PreexistingEntityException("TbDiccionario " + tbDiccionario + " already exists.", ex);
      }
      throw ex;
    } finally {
      if (em != null) {
        em.close();
      }
    }
  }

  public void edit(TbDiccionario tbDiccionario) throws NonexistentEntityException, Exception {
    EntityManager em = null;
    try {
      em = getEntityManager();
      em.getTransaction().begin();
      tbDiccionario = em.merge(tbDiccionario);
      em.getTransaction().commit();
    } catch (Exception ex) {
      String msg = ex.getLocalizedMessage();
      if (msg == null || msg.length() == 0) {
        String id = tbDiccionario.getId();
        if (findTbDiccionario(id) == null) {
          throw new NonexistentEntityException("The tbDiccionario with id " + id + " no longer exists.");
        }
      }
      throw ex;
    } finally {
      if (em != null) {
        em.close();
      }
    }
  }

  public void destroy(String id) throws NonexistentEntityException {
    EntityManager em = null;
    try {
      em = getEntityManager();
      em.getTransaction().begin();
      TbDiccionario tbDiccionario;
      try {
        tbDiccionario = em.getReference(TbDiccionario.class, id);
        tbDiccionario.getId();
      } catch (EntityNotFoundException enfe) {
        throw new NonexistentEntityException("The tbDiccionario with id " + id + " no longer exists.", enfe);
      }
      em.remove(tbDiccionario);
      em.getTransaction().commit();
    } finally {
      if (em != null) {
        em.close();
      }
    }
  }

  public List<TbDiccionario> findTbDiccionarioEntities() {
    return findTbDiccionarioEntities(true, -1, -1);
  }

  public List<TbDiccionario> findTbDiccionarioEntities(int maxResults, int firstResult) {
    return findTbDiccionarioEntities(false, maxResults, firstResult);
  }

  private List<TbDiccionario> findTbDiccionarioEntities(boolean all, int maxResults, int firstResult) {
    EntityManager em = getEntityManager();
    try {
      CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
      cq.select(cq.from(TbDiccionario.class));
      Query q = em.createQuery(cq);
      if (!all) {
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
      }
      return q.getResultList();
    } finally {
      em.close();
    }
  }

  public TbDiccionario findTbDiccionario(String id) {
    EntityManager em = getEntityManager();
    try {
      return em.find(TbDiccionario.class, id);
    } finally {
      em.close();
    }
  }

  public int getTbDiccionarioCount() {
    EntityManager em = getEntityManager();
    try {
      CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
      Root<TbDiccionario> rt = cq.from(TbDiccionario.class);
      cq.select(em.getCriteriaBuilder().count(rt));
      Query q = em.createQuery(cq);
      return ((Long) q.getSingleResult()).intValue();
    } finally {
      em.close();
    }
  }
  
}
