/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.diccionarioapp.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.diccionarioapp.dao.TbDiccionarioJpaController;
import root.diccionarioapp.entity.TbDiccionario;

/**
 *
 * @author surro
 */
@WebServlet(name = "diccionarioController", urlPatterns = {"/diccionarioController"})
public class diccionarioController extends HttpServlet {

  int i = 0;

  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try ( PrintWriter out = response.getWriter()) {
      /* TODO output your page here. You may use following sample code. */
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet diccionarioController</title>");
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet diccionarioController at " + request.getContextPath() + "</h1>");
      out.println("</body>");
      out.println("</html>");
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    TbDiccionarioJpaController dao = new TbDiccionarioJpaController();
    try {

      String palabra = request.getParameter("palabra");

      LocalDate fe = LocalDate.now();
      DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yyyy");
      String fecha = fe.format(formato);

      String definicion = "definicion";

      String id = String.valueOf(i++);

      TbDiccionario dic = new TbDiccionario();
      dic.setId(id);
      dic.setPalabra(palabra);
      dic.setFecha(fecha);
      dic.setDefinicion(definicion);

      dao.create(dic);
    } catch (Exception ex) {
      Logger.getLogger(diccionarioController.class.getName()).log(Level.SEVERE, null, ex);
    }

    List<TbDiccionario> lista = dao.findTbDiccionarioEntities();
    request.setAttribute("lista", lista);
    request.getRequestDispatcher("resultado.jsp").forward(request, response);

  }

  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
