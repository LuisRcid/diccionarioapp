/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.diccionarioapp.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author surro
 */
@Entity
@Table(name = "tb_diccionario")
@NamedQueries({
  @NamedQuery(name = "TbDiccionario.findAll", query = "SELECT t FROM TbDiccionario t"),
  @NamedQuery(name = "TbDiccionario.findById", query = "SELECT t FROM TbDiccionario t WHERE t.id = :id"),
  @NamedQuery(name = "TbDiccionario.findByPalabra", query = "SELECT t FROM TbDiccionario t WHERE t.palabra = :palabra"),
  @NamedQuery(name = "TbDiccionario.findByFecha", query = "SELECT t FROM TbDiccionario t WHERE t.fecha = :fecha"),
  @NamedQuery(name = "TbDiccionario.findByDefinicion", query = "SELECT t FROM TbDiccionario t WHERE t.definicion = :definicion")})
public class TbDiccionario implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 2147483647)
  @Column(name = "id")
  private String id;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 2147483647)
  @Column(name = "palabra")
  private String palabra;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 2147483647)
  @Column(name = "fecha")
  private String fecha;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 2147483647)
  @Column(name = "definicion")
  private String definicion;

  public TbDiccionario() {
  }

  public TbDiccionario(String id) {
    this.id = id;
  }

  public TbDiccionario(String id, String palabra, String fecha, String definicion) {
    this.id = id;
    this.palabra = palabra;
    this.fecha = fecha;
    this.definicion = definicion;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPalabra() {
    return palabra;
  }

  public void setPalabra(String palabra) {
    this.palabra = palabra;
  }

  public String getFecha() {
    return fecha;
  }

  public void setFecha(String fecha) {
    this.fecha = fecha;
  }

  public String getDefinicion() {
    return definicion;
  }

  public void setDefinicion(String definicion) {
    this.definicion = definicion;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof TbDiccionario)) {
      return false;
    }
    TbDiccionario other = (TbDiccionario) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "root.diccionarioapp.entity.TbDiccionario[ id=" + id + " ]";
  }
  
}
