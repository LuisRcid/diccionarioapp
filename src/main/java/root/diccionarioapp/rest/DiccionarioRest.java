package root.diccionarioapp.rest;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.diccionarioapp.dao.TbDiccionarioJpaController;
import root.diccionarioapp.entity.TbDiccionario;

@Path("Diccionario")
public class DiccionarioRest {
  
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response listapalabras(){
    TbDiccionarioJpaController dao = new TbDiccionarioJpaController();
    List<TbDiccionario> lista = dao.findTbDiccionarioEntities();
    return Response.ok(200).entity(lista).build();
  }
}
