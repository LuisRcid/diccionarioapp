<%-- 
    Document   : resultado
    Created on : 23-10-2021, 19:45:43
    Author     : surro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion Final</title>
    </head>
    <body>
        <header>
            <h1 style="font-size: 72px; text-align: center">Diccionario</h1>
        </header>
        <div style="text-align: center">
            <label>Palabra: <input type="text" id="resultado" value=""></label>
            <br>
            <label>Definicion: <input type="text" id="definicion" value=""></label>
        </div>
    </body>
</html>
