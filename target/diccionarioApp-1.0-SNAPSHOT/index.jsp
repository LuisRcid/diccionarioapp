<%-- 
    Document   : index
    Created on : 23-10-2021, 16:16:58
    Author     : surro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion Final</title>
    </head>
    <body>
        <header>
            <h1 style="font-size: 72px; text-align: center">Diccionario</h1>
            <p style="font-size: 34px; text-align: center"> Ingrese una palabra para buscar la definicion </p>
        </header>
        <div style="text-align: center">
            <form name="form" action="diccionarioController" method="POST">
                <input type="text" id="palabra" placeholder="Escriba aqui su palabra" autofocus required>
                <input type="submit" id="btnEnviar" value="Buscar">
            </form>
        </div>
        <footer>
            <p style="text-align: center">Fuente: Oxford Dictionary</p>
        </footer>
    </body>
</html>
